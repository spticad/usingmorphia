package Entity;
import com.google.code.morphia.annotations.Entity;

/**
 * Created by maksim on 06.04.14.
 */
public class Account extends BaseEntity {
    private String name;

    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
}
