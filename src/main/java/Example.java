import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import Entity.Account;
import Entity.Address;
import Entity.Students;
import com.google.code.morphia.Datastore;
import com.google.code.morphia.Key;
import com.google.code.morphia.Morphia;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
/**
 * Created by maksim on 06.04.14.
 */
public class Example {

    public static void main(String[] args) throws UnknownHostException,MongoException {
        String dbName= new String("bank");
        Mongo mongo= new Mongo();
        Morphia morphia= new Morphia();
        Datastore datastore = morphia.createDatastore(mongo,dbName);
        morphia.mapPackage("Entity");

        Address address = new Address();
        address.setTown("Gatchina");
        address.setStreet("Izotova");
        address.setNumber("19");
        address.setPostcode("188300");

        Account account= new Account();
        account.setName("Maksim");

        List<Account> accounts= new ArrayList<Account>();
        accounts.add(account);

        Students student =new Students();
        student.setName("Maksim");
        student.setAddress(address);

        Key<Students> savedStudent = datastore.save(student);
        System.out.println(savedStudent.getId());
        System.out.println(savedStudent.getKind());


    }
}
