package Entity;

import org.bson.types.ObjectId;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Property;
import com.google.code.morphia.annotations.Version;

/**
 * Created by maksim on 06.04.14.
 */
public abstract class BaseEntity {
    @Id
    @Property("id")
    protected ObjectId id;

    @Version
    @Property("version")
    private long version;

    public BaseEntity(){
        super();
    }
    public ObjectId getId(){
        return id;
    }
    public void setId(ObjectId id){
        this.id=id;
    }
    public long getVersion(){
        return version;
    }
    public void setVersion(Long version){
        this.version=version;
    }

}
