package Entity;

import java.util.List;
import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Entity;

/**
 * Created by maksim on 06.04.14.
 */

@Entity
public class Students extends BaseEntity{
    private String name;
    private List<Account> accounts;
    @Embedded
    private Address address;

            public String getName() {
       return name;
            }
    public void setName(String name){
        this.name=name;
    }
    public List<Account> getAccounts(){
        return accounts;

    }
    public Address getAddress(){
        return address;
    }
    public void setAddress(Address address){
        this.address=address;
    }

}
